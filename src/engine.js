// JavaScript Morse code translation module, powered by the "beep" command.

module.exports.Beep = function (characters, unitLength, freq, CLI) {
  // Synchronous command execution for "beep" from "child_process".
  const exec = require('child_process').execSync;

  // Temporarily store a "chunk" of the same morse code characters here.
  let tempArray = [];
  // Final location for "chunked" Morse code.
  // e.g. ['.', '.', ' ', '-', '.', '-', '-'] ---> ['..', ' ', '-', '.', '--']
  let finalArray = [];

  // Sort each Morse character into chunks of duplicates.
  characters.forEach((item, index) => {
    // If tempArray is empty OR current character is the same as the last...
    if (
      (item && !tempArray.length) ||
      item == tempArray[tempArray.length - 1]
    ) {
      // Add the current character to tempArray.
      tempArray.push(item);
    } else {
      // On different character, push and reset tempArray.
      finalArray.push(tempArray.join(''));
      tempArray = [item];
    }
  });
  // Finally push any remaining characters in tempArray to finalArray.
  finalArray.push(tempArray.join(''));

  // Reoccuring function that can be delayed. Accesses beep console command.
  function doBeep(repeats, length) {
    exec(`beep -r ${repeats} -f ${freq} -l ${length}`);
    nextBeeps();
  }

  // Store current location of finalArray.
  let index = -1;
  // Function to process next set of beeps.
  function nextBeeps() {
    // Increment index on each run.
    index++;
    // Once the last chunk has been played...
    if (index >= finalArray.length) {
      // Prompt the interpreter in index.js, which has been waiting for this engine to finish.
      CLI.prompt();
      // Prevent this function from running again.
      return;
    }
    // Shortcut to current "chunk" of Morse code to process.
    const latest = finalArray[index];

    // Get first character of the chunk, and switch for different characters.
    switch (latest.charAt()) {
      default:
        // Ignore anything other than ".", "-" or " ".
        console.log(`Ignoring unsupported character: ${latest.charAt()}`);
        nextBeeps();
        break;
      // setTimeout() used to provide at least 1 unit of space between operations.
      // Each operation extends their length based on no. of character in chunk.
      case '.':
        // On "dit", beep for 1 unit.
        setTimeout(doBeep, unitLength, latest.length, unitLength);
        break;
      case '-':
        // On "dah", beep for 3 units.
        setTimeout(doBeep, unitLength, latest.length, unitLength * 3);
        break;
      case ' ':
        // On space, rerun function after 7 units.
        return setTimeout(nextBeeps, unitLength * 7 * latest.length);
        break;
    }
  }

  console.log(finalArray);

  // Kickstart recurring beep function.
  nextBeeps();
};
