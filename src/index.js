// Commandline interpreter that calls a beeping engine after user Morse code input.

// readline used to accept responses from user.
const readline = require('readline');
// Send user input to Beep() function from engine.
const engine = require('./engine.js');

const morseCodes = {
  a: '.-',
  b: '-...',
  c: '-.-.',
  d: '-..',
  e: '.',
  f: '..-.',
  g: '--.',
  h: '....',
  i: '..',
  j: '.---',
  k: '-.-',
  l: '.-..',
  m: '--',
  n: '-.',
  o: '---',
  p: '.--.',
  q: '--.-',
  r: '.-.',
  s: '...',
  t: '-',
  u: '..-',
  v: '...-',
  w: '.--',
  x: '-..-',
  y: '-.--',
  z: '--..',
  '1': '.----',
  '2': '..---',
  '3': '...--',
  '4': '....-',
  '5': '.....',
  '6': '-....',
  '7': '--...',
  '8': '---..',
  '9': '----.',
  '0': '-----',
  ' ': ' ',
};

// Specify how long each unit should be, and the frequency they beep at.
const unitMs = 50;
const frequencyHz = 1000;

// Create a standard interface with readline.
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: 'morse> ',
});
// Ensure the prompt cycle is started.
rl.prompt();

// Once the user has issued text and hit enter...
rl.on('line', (line) => {
  // Send user text, unit length, frequency, and the current readline object to engine.
  // This prompt is called again from within the engine, once it has finished running.
  //engine.Beep(line, unitMs, frequencyHz, rl);
  const alphanumeric = line.replace(/[.-]/g, '');
  const lineMin = line.length / 2;
  const lineArray = line.toLowerCase().split('');

  if (alphanumeric.length > lineMin) {
    console.log('---> Assuming input is alphanumeric, translating...');

    const translated = lineArray.map(function (e) {
      return morseCodes[e] || null;
    });

    engine.Beep(translated, unitMs, frequencyHz, rl);
  } else {
    console.log('---> Assuming line is Morse code.');
    engine.Beep(lineArray, unitMs, frequencyHz, rl);
  }
}).on('close', () => {
  console.log('---X---');
  process.exit();
});
